package com.lionseal.buscaminas;

import java.util.ArrayList;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.lionseal.buscaminas.utiles.Cons;
import com.lionseal.buscaminas.utiles.Textures;

public class AreaReloj implements IUpdateHandler {

	private float x, y;
	private TableroBuscaminas tablero;
	private ArrayList<AnimatedSprite> segundos;
	private float time;
	private int clock = 0;
	private float zoom;

	public AreaReloj(TableroBuscaminas t, float pX, float pY, float z) {
		tablero = t;
		x = pX;
		y = pY;
		zoom = z * 2;
		segundos = new ArrayList<>();
		VertexBufferObjectManager vbom = tablero.getVertexBufferObjectManager();
		AnimatedSprite s = new AnimatedSprite(x, y, Textures.get().tiledTexture(Cons.RELOJ), vbom);
		s.setScaleCenter(0, 0);
		s.setScale(zoom);
		segundos.add(s);
		tablero.hud.attachChild(s);
		s = new AnimatedSprite(x + Cons.REL_WIDTH * zoom, y, Textures.get().tiledTexture(Cons.RELOJ), vbom);
		s.setScaleCenter(0, 0);
		s.setScale(zoom);
		segundos.add(s);
		tablero.hud.attachChild(s);
		s = new AnimatedSprite(x + (2 * Cons.REL_WIDTH * zoom), y, Textures.get().tiledTexture(Cons.RELOJ), vbom);
		s.setScaleCenter(0, 0);
		s.setScale(zoom);
		segundos.add(s);
		tablero.hud.attachChild(s);
		drawClock();
	}

	private void drawClock() {
		int size = segundos.size();
		String value = "I" + clock + "D";
		int valueLength = value.length();
		for (int i = size; i < valueLength; i++) {
			VertexBufferObjectManager vbom = tablero.getVertexBufferObjectManager();
			AnimatedSprite s = new AnimatedSprite(x, y, Textures.get().tiledTexture(Cons.RELOJ), vbom);
			s.setScaleCenter(0, 0);
			s.setScale(zoom);
			segundos.add(1, s);
			tablero.hud.attachChild(s);
			size++;
		}
		for (int i = 1; i < size; i++) {
			AnimatedSprite s = segundos.get(i);
			s.setX(x + i * Cons.REL_WIDTH * zoom);
			s.setCurrentTileIndex(Cons.valueOf("T" + value.charAt(i)).valor);
		}
	}

	public void start() {
		tablero.hud.registerUpdateHandler(this);
	}

	public void stop() {
		tablero.hud.unregisterUpdateHandler(this);
	}

	public void setTime(int t) {
		time = t;
		clock = t;
		drawClock();
	}

	public int getTime() {
		return clock;
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		time += pSecondsElapsed;
		if (time - clock > 1 && clock < 99999) {
			clock++;
			drawClock();
		}
	}

	@Override
	public void reset() {
	}
}
