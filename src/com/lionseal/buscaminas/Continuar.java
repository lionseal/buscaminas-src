package com.lionseal.buscaminas;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.lionseal.buscaminas.DBBuscaminas.Tableros;
import com.lionseal.buscaminas.utiles.Dificultad;
import com.lionseal.buscaminas.utiles.Tama�o;

public class Continuar extends Dialog {

	private Activity activity;

	public Continuar(Activity a, boolean newGame) {
		super(a);
		activity = a;
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.saved_buscaminas);

		if (newGame)
			startNewGame();
		else
			loadListView();
	}

	private void loadListView() {
		ListView l = (ListView) findViewById(R.id.listView);
		final List<String[]> juegos = new ArrayList<>();

		DBBuscaminas dbb = new DBBuscaminas(activity);
		SQLiteDatabase db = dbb.getReadableDatabase();

		Cursor c = db.query(Tableros.TABLE_NAME, new String[] { Tableros.date, Tableros.seed, Tableros.width, Tableros.height, Tableros.minas, Tableros.dif, Tableros.size }, null, null, null, null,
				Tableros.date + " DESC");
		if (c.moveToFirst()) {
			c.moveToPrevious();
			while (c.moveToNext()) {
				juegos.add(new String[] { c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5) + "", c.getString(6) + "" });
			}
		}
		db.close();

		ArrayAdapter<String[]> arrayAdapter = new ArrayAdapter<String[]>(activity, android.R.layout.simple_list_item_1, juegos) {
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);
				TextView text1 = (TextView) view.findViewById(android.R.id.text1);
				text1.setTextColor(Color.parseColor("#000000"));
				text1.setText(juegos.get(position)[0]);
				return view;
			}
		};

		l.setAdapter(arrayAdapter);
		l.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				startGame(juegos.get(position));
			}
		});
	}

	private void startNewGame() {
		SharedPreferences settings = activity.getSharedPreferences(Configuracion.PREFS_CONFIG, 0);
		String difficulty = settings.getInt(Configuracion.DIFFICULTY, 0) + "";
		String size = settings.getInt(Configuracion.SIZE, 0) + "";
		String width = settings.getString(Configuracion.WIDTH, "" + Tama�o.CHICO.width);
		String height = settings.getString(Configuracion.HEIGHT, "" + Tama�o.CHICO.height);
		String minas = settings.getString(Configuracion.MINAS, "" + (int) ((Tama�o.CHICO.width * Tama�o.CHICO.width) / Dificultad.PRINCIPIANTE.proporcion));
		startGame(new String[] { "Juego nuevo", null, width, height, minas, difficulty, size });
	}

	private void startGame(String[] args) {
		Intent i = new Intent(activity, TableroBuscaminas.class);
		i.putExtra(TableroBuscaminas.SEED, args[1]);
		i.putExtra(Configuracion.WIDTH, Integer.valueOf(args[2]));
		i.putExtra(Configuracion.HEIGHT, Integer.valueOf(args[3]));
		i.putExtra(Configuracion.MINAS, Integer.valueOf(args[4]));
		i.putExtra(Configuracion.DIFFICULTY, Integer.valueOf(args[5]));
		i.putExtra(Configuracion.SIZE, Integer.valueOf(args[6]));

		activity.startActivity(i);
		if (isShowing())
			dismiss();
	}

}
