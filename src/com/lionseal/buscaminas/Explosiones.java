package com.lionseal.buscaminas;

import java.util.ArrayList;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.AnimatedSprite;

import com.lionseal.buscaminas.utiles.Cons;
import com.lionseal.buscaminas.utiles.Textures;
import com.lionseal.buscaminas.utiles.Utiles;

public class Explosiones implements IUpdateHandler {
	private ArrayList<AnimatedSprite> explosiones;
	private ArrayList<Casilla> minas;
	private TableroBuscaminas tablero;
	private AnimatedSprite explotando;

	public Explosiones(TableroBuscaminas t) {
		tablero = t;
		explosiones = new ArrayList<>();
		minas = new ArrayList<>();
		tablero.scene.registerUpdateHandler(this);
	}

	public void addExplosion(Casilla c, String tama�o) {
		float pX = c.x * Cons.CAS_WIDTH + Cons.CAS_WIDTH / 2 - Cons.EXP_WIDTH / 2;
		float pY = c.y * Cons.CAS_HEIGHT + Cons.CAS_HEIGHT / 2 - Cons.EXP_HEIGHT / 2;
		AnimatedSprite sprite = new AnimatedSprite(pX, pY, Textures.get().tiledTexture(tama�o),
				tablero.getVertexBufferObjectManager());
		explosiones.add(sprite);
		minas.add(c);
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		if (explosiones.isEmpty())
			return;
		if (explotando == null) {
			explotando = explosiones.get(0);
			minas.get(0).doubleClicked();
			tablero.scene.attachChild(explotando);
			explotando.animate(50);
		}
		if (explotando.getCurrentTileIndex() == 10) {
			Utiles.removeSprite(explotando);
			explosiones.remove(0);
			minas.remove(0);
			explotando = null;
		}
	}

	@Override
	public void reset() {
	}
}
