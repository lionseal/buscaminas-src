package com.lionseal.buscaminas;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ComoJugar extends FragmentActivity implements OnPageChangeListener {

	private static final int NUM_PAGES = 5;
	private ViewPager mPager;
	private PagerAdapter mPagerAdapter;
	private ArrayList<PaginaComoJugar> pages;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.como_jugar_buscaminas);

		pages = new ArrayList<>();
		for (int i = 0; i < NUM_PAGES; i++) {
			PaginaComoJugar p = new PaginaComoJugar();
			p.setPosition(i);
			p.setRetainInstance(false);
			pages.add(p);
		}
		mPager = (ViewPager) findViewById(R.id.pager_como_jugar);
		mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
		mPager.setAdapter(mPagerAdapter);
		mPager.setOnPageChangeListener(this);
	}

	protected void onResume() {
		super.onResume();
		mPager.setCurrentItem(0);
	}

	public void onBackPressed() {
		if (mPager.getCurrentItem() == 0) {
			super.onBackPressed();
		} else {
			int item = mPager.getCurrentItem();
			findViewById(getResources().getIdentifier("ind" + item, "id", getPackageName())).setVisibility(View.INVISIBLE);
			item--;
			findViewById(getResources().getIdentifier("ind" + item, "id", getPackageName())).setVisibility(View.VISIBLE);
			mPager.setCurrentItem(item);
		}
	}

	protected void onStop() {
		mPager.destroyDrawingCache();
		mPager.setAdapter(null);
		super.onStop();
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		public Fragment getItem(int position) {
			return pages.get(position);
		}

		public int getCount() {
			return NUM_PAGES;
		}
	}

	public static class PaginaComoJugar extends Fragment {
		private int pos;

		public PaginaComoJugar() {
			super();
		}

		public void setPosition(int p) {
			pos = p;
		}

		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.como_jugar_fragment, container, false);
			TextView text = (TextView) rootView.findViewById(R.id.text_page_como_jugar);
			text.setText(getResources().getString(getResources().getIdentifier("text_como_jugar_" + pos, "string", getActivity().getPackageName())));
			ImageView image = (ImageView) rootView.findViewById(R.id.image_page_como_jugar);
			image.setImageDrawable(getResources().getDrawable(getResources().getIdentifier("my_cj" + pos, "drawable", getActivity().getPackageName())));
			return rootView;
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int item) {
		if (item - 1 > -1)
			findViewById(getResources().getIdentifier("ind" + (item - 1), "id", getPackageName())).setVisibility(View.INVISIBLE);
		findViewById(getResources().getIdentifier("ind" + item, "id", getPackageName())).setVisibility(View.VISIBLE);
		if (item + 1 < NUM_PAGES)
			findViewById(getResources().getIdentifier("ind" + (item + 1), "id", getPackageName())).setVisibility(View.INVISIBLE);
	}
}
