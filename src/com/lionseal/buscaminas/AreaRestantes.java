package com.lionseal.buscaminas;

import java.util.ArrayList;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.lionseal.buscaminas.utiles.Cons;
import com.lionseal.buscaminas.utiles.Textures;
import com.lionseal.buscaminas.utiles.Utiles;

public class AreaRestantes {
	private float x, y;
	private TableroBuscaminas tablero;
	private ArrayList<AnimatedSprite> minas, casillas;
	public int minasRestantes, casillasRestantes;
	private float zoom;

	public AreaRestantes(TableroBuscaminas t, float pX, float pY,float z, int m, int c) {
		tablero = t;
		zoom = z;
		x = pX - Cons.CAS_WIDTH * zoom;
		y = pY;
		minas = new ArrayList<>();
		casillas = new ArrayList<>();
		minasRestantes = m;
		casillasRestantes = c;

		VertexBufferObjectManager vbom = tablero.getVertexBufferObjectManager();

		AnimatedSprite s = new AnimatedSprite(x, y - Cons.REL_HEIGHT * zoom, Textures.get().tiledTexture(Cons.CASILLAS), vbom);
		s.setCurrentTileIndex(Cons.MINA.valor);
		s.setScaleCenter(0, 0);
		s.setScale(zoom);
		tablero.hud.attachChild(s);
		s = new AnimatedSprite(x, y, Textures.get().tiledTexture(Cons.CASILLAS), vbom);
		s.setCurrentTileIndex(Cons.NORMAL.valor);
		s.setScaleCenter(0, 0);
		s.setScale(zoom);
		tablero.hud.attachChild(s);

		int size = ("T" + minasRestantes + "D").length();
		for (int i = 0; i < size; i++) {
			s = new AnimatedSprite(x - Cons.REL_WIDTH * zoom, y - Cons.REL_HEIGHT * zoom, Textures.get().tiledTexture(Cons.RELOJ), vbom);
			s.setCurrentTileIndex(Cons.TD.valor);
			s.setScaleCenter(0, 0);
			s.setScale(zoom);
			minas.add(s);
			tablero.hud.attachChild(s);
		}
		size = ("T" + casillasRestantes + "D").length();
		for (int i = 0; i < size; i++) {
			s = new AnimatedSprite(x - Cons.REL_WIDTH * zoom, y, Textures.get().tiledTexture(Cons.RELOJ), vbom);
			s.setCurrentTileIndex(Cons.TD.valor);
			s.setScaleCenter(0, 0);
			s.setScale(zoom);
			casillas.add(s);
			tablero.hud.attachChild(s);
		}
		drawMinas();
		drawCasillas();
	}

	private void drawMinas() {
		String m = "I" + minasRestantes + "D";
		if (m.length() < minas.size()) {
			Utiles.removeSprite(minas.get(1));
			minas.remove(1);
		}
		int size = minas.size() - 1;
		for (int i = size; i > -1; i--) {
			AnimatedSprite s = minas.get(i);
			s.setX(x - ((size - i) * Cons.REL_WIDTH * zoom) - Cons.REL_WIDTH * zoom);
			// s = Utiles.cambiarSprite(tablero.scene, s, "T" + m.charAt(i));
			s.setCurrentTileIndex(Cons.valueOf("T" + m.charAt(i)).valor);
		}
	}

	private void drawCasillas() {
		String c = "I" + casillasRestantes + "D";
		if (c.length() < casillas.size()) {
			Utiles.removeSprite(casillas.get(1));
			casillas.remove(1);
		}
		int size = casillas.size() - 1;
		for (int i = size; i > -1; i--) {
			AnimatedSprite s = casillas.get(i);
			s.setX(x - ((size - i) * Cons.REL_WIDTH * zoom) - Cons.REL_WIDTH * zoom);
			// s = Utiles.cambiarSprite(tablero.scene, s, "T" + c.charAt(i));
			s.setCurrentTileIndex(Cons.valueOf("T" + c.charAt(i)).valor);
		}
	}

	public void descontarMinas() {
		minasRestantes--;
		drawMinas();
	}

	public void descontarCasillas() {
		casillasRestantes--;
		drawCasillas();
	}
}
