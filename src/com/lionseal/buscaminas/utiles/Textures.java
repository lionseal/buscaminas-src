package com.lionseal.buscaminas.utiles;

import java.util.HashMap;

import org.andengine.opengl.texture.region.ITiledTextureRegion;

import android.util.Log;

public class Textures {
	private static final Textures instance = new Textures();

	private HashMap<String, ITiledTextureRegion> tiledTextureBank = new HashMap<>();

	public static Textures get() {
		return instance;
	}

	public ITiledTextureRegion tiledTexture(String key) {
		ITiledTextureRegion tex = tiledTextureBank.get(key);
		if (tex == null) {
			Log.e("Texture", "Texture missing: " + key);
		}
		return tex.deepCopy();
	}

	public void putTiledTexture(String key, ITiledTextureRegion t) {
		if (t != null)
			tiledTextureBank.put(key, t);
	}
}
