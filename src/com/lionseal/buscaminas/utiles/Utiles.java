package com.lionseal.buscaminas.utiles;

import org.andengine.entity.sprite.Sprite;

import android.app.Activity;
import android.widget.Toast;

public class Utiles {

	public static void Toast(final Activity context, final String text) {
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(context, text, Toast.LENGTH_LONG).show();
			}
		});
	}

	public static void removeSprite(Sprite sprite) {
		if (sprite != null) {
			sprite.detachSelf();
			sprite.dispose();
			sprite = null;
		}
	}
}
