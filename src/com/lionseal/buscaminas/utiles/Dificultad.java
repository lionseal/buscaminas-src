package com.lionseal.buscaminas.utiles;


public enum Dificultad {
	PRINCIPIANTE(10), INTERMEDIO(6.4), AVANZADO(5.12), EXPERTO(4.8), DETECTOR(4.0);

	public double proporcion;

	private Dificultad(double p) {
		proporcion = p;
	}
}
