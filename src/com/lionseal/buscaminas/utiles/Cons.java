package com.lionseal.buscaminas.utiles;

public enum Cons {
	NADA(0), UNO(1), DOS(2), TRES(3), CUATRO(4), CINCO(5), SEIS(6), SIETE(7), OCHO(8), MINA(9), ROJA(10), NORMAL(11), INVERTIDA(
			12), BANDERA(13), CARA_NORMAL(0), CARA_BOCA(1), CARA_GANO(2), CARA_MUERTO(3), TI(0), T0(1), T1(2), T2(3), T3(4), T4(5), T5(
			6), T6(7), T7(8), T8(9), T9(10), TD(11), BIG(0), SMALL(1);

	public static final int CAS_WIDTH = 20, CAS_HEIGHT = 20;
	public static final int CARA_WIDTH = 40, CARA_HEIGHT = 40;
	public static final int REL_WIDTH = 14, REL_HEIGHT = 20;
	public static final int EXP_WIDTH = 32, EXP_HEIGHT = 32, EXP_COL = 11, EXP_ROW = 2;
	public static final String CASILLAS = "CASILLAS", CARAS = "CARAS", RELOJ = "RELOJ", EXPLOSIONES = "EXPLOSIONES";

	public final int valor;

	private Cons(int val) {
		valor = val;
	}
}
