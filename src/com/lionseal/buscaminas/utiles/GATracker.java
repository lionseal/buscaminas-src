package com.lionseal.buscaminas.utiles;

import java.util.HashMap;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.lionseal.buscaminas.R;

public class GATracker {

	public enum TrackerName {
		APP_TRACKER, GLOBAL_TRACKER
	}

	static HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public static synchronized Tracker getTracker(Context c, TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(c);
			Tracker t;
			if (trackerId == TrackerName.APP_TRACKER) {
				t = analytics.newTracker(R.xml.app_tracker);
			} else {
				t = analytics.newTracker(R.xml.global_tracker);
			}
			mTrackers.put(trackerId, t);
		}
		return mTrackers.get(trackerId);
	}
}
