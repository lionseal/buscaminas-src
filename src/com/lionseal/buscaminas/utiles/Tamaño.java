package com.lionseal.buscaminas.utiles;

public enum Tama�o {
	CHICO(10, 10), MEDIANO(16, 16), GRANDE(30, 16), ENORME(30, 30), EPICO(60, 30);

	public int width, height;

	private Tama�o(int w, int h) {
		width = w;
		height = h;
	}
}
