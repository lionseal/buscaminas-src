package com.lionseal.buscaminas;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.View.OnClickListener;

import com.lionseal.buscaminas.utiles.Dificultad;
import com.lionseal.buscaminas.utiles.Tama�o;

public class Configuracion extends Dialog implements OnClickListener, OnItemSelectedListener {

	public static final String PREFS_CONFIG = "settings";
	public static final String DIFFICULTY = "difficulty";
	public static final String SIZE = "size";
	public static final String HEIGHT = "height";
	public static final String WIDTH = "width";
	public static final String MINAS = "minas";
	public static final String CUSTOM = "custom";
	public static final String SIGN_IN = "signIn";
	private int difficulty, size, width, height, minas;
	private Context context;
	private TextView textSize, textMinas;

	public Configuracion(Context c) {
		super(c);
		context = c;
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.configuracion_popup);
		SharedPreferences settings = context.getSharedPreferences(PREFS_CONFIG, 0);
		difficulty = settings.getInt(DIFFICULTY, 0);
		size = settings.getInt(SIZE, 0);
		textSize = (TextView) findViewById(R.id.popup_size);
		textMinas = (TextView) findViewById(R.id.popup_minas);
		setUpSpinners();
		findViewById(R.id.button_guardar).setOnClickListener(this);
	}

	private void setUpSpinners() {
		Spinner spinner = (Spinner) findViewById(R.id.spinner_size);
		List<String> list = new ArrayList<String>();
		list.add(context.getResources().getString(R.string.text_chico));
		list.add(context.getResources().getString(R.string.text_medio));
		list.add(context.getResources().getString(R.string.text_grande));
		list.add(context.getResources().getString(R.string.text_enorme));
		list.add(context.getResources().getString(R.string.text_epico));
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(R.drawable.spinner_custom);
		spinner.setAdapter(dataAdapter);
		spinner.setSelection(size);
		spinner.setOnItemSelectedListener(this);

		spinner = (Spinner) findViewById(R.id.spinner_difficulty);
		list = new ArrayList<String>();
		list.add(context.getResources().getString(R.string.text_principiante));
		list.add(context.getResources().getString(R.string.text_intermedio));
		list.add(context.getResources().getString(R.string.text_avanzado));
		list.add(context.getResources().getString(R.string.text_experto));
		list.add(context.getResources().getString(R.string.text_detector));
		dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(R.drawable.spinner_custom);
		spinner.setAdapter(dataAdapter);
		spinner.setSelection(difficulty);
		spinner.setOnItemSelectedListener(this);
	}

	private void setText() {
		double proporcion;
		Tama�o t = Tama�o.values()[size];
		width = t.width;
		height = t.height;
		proporcion = Dificultad.values()[difficulty].proporcion;
		minas = (int) (width * height / proporcion);

		textSize.setText(width + "x" + height);
		textMinas.setText("" + minas);
	}

	@Override
	public void onClick(View v) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_CONFIG, 0);
		Editor editor = settings.edit();
		editor.putInt(DIFFICULTY, difficulty);
		editor.putInt(SIZE, size);
		editor.putString(WIDTH, "" + width);
		editor.putString(HEIGHT, "" + height);
		editor.putString(MINAS, "" + minas);
		editor.commit();
		dismiss();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
		switch (parent.getId()) {
		case R.id.spinner_size:
			size = p;
			break;
		case R.id.spinner_difficulty:
			difficulty = p;
			break;
		default:
			Log.d("NOT FOUND", "" + R.id.spinner_size);
		}
		setText();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}

}
