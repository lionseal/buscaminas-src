package com.lionseal.buscaminas;

import java.util.ArrayList;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.lionseal.buscaminas.DBBuscaminas.Records;
import com.lionseal.buscaminas.utiles.Dificultad;
import com.lionseal.buscaminas.utiles.GATracker;
import com.lionseal.buscaminas.utiles.Tama�o;
import com.lionseal.buscaminas.utiles.GATracker.TrackerName;

public class LeaderboardBuscaminas extends ActionBarActivity implements OnPageChangeListener, TabListener, GameHelperListener {

	public static final int AUTO_SIGN_IN_ON = 1;
	public static final int AUTO_SIGN_IN_OFF = 0;

	private PagerAdapter pagerAdapter;
	private ViewPager pager;
	private WorldWideScore scoreWorld;
	private PersonalScore scorePersonal;
	private static ArrayList<Integer> savedScores;
	private GameHelper G_HELPER;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.leaderboard_buscaminas);
		pagerAdapter = new PagerAdapter(getSupportFragmentManager());
		pager = (ViewPager) findViewById(R.id.pager_como_jugar);
		pager.setOnPageChangeListener(this);
		pager.setAdapter(pagerAdapter);

		loadScores();
		scoreWorld = new WorldWideScore();
		scorePersonal = new PersonalScore();

		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.addTab(actionBar.newTab().setText(getString(R.string.tab_world)).setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText(getString(R.string.tab_personal)).setTabListener(this));

		G_HELPER = new GameHelper(this, GameHelper.CLIENT_GAMES);
		G_HELPER.setup(this);
		SharedPreferences settings = getSharedPreferences(Configuracion.PREFS_CONFIG, 0);
		G_HELPER.setMaxAutoSignInAttempts(settings.getInt(Configuracion.SIGN_IN, LeaderboardBuscaminas.AUTO_SIGN_IN_ON));
		GATracker.getTracker(this, TrackerName.APP_TRACKER);
	}

	protected void onStart() {
		super.onStart();
		G_HELPER.onStart(this);
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	protected void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		G_HELPER.onActivityResult(request, response, data);
	}

	protected void onStop() {
		super.onStop();
		G_HELPER.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	public class PagerAdapter extends FragmentPagerAdapter {

		public PagerAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
		}

		@Override
		public Fragment getItem(int arg0) {
			switch (arg0) {
			case 0:
				return scoreWorld;
			default:
				return scorePersonal;
			}
		}

		@Override
		public int getCount() {
			return 2;
		}
	}

	public void loadScores() {
		DBBuscaminas dbb = new DBBuscaminas(this);
		SQLiteDatabase db = dbb.getReadableDatabase();
		Cursor c = db.query(Records.TABLE_NAME, new String[] { Records.dif, Records.size, Records.score, Records.date }, null, null, null, null, Records.dif + " DESC," + Records.size + " DESC");
		if (c.moveToFirst()) {
			savedScores = new ArrayList<>();
			c.moveToPrevious();
			while (c.moveToNext()) {
				int dif, size, score;
				dif = c.getInt(0);
				size = c.getInt(1);
				score = c.getInt(2);
				savedScores.add(dif);
				savedScores.add(size);
				savedScores.add(score);
			}
		}
	}

	public static class PersonalScore extends Fragment {
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.score_personal_buscaminas, container, false);
			ViewGroup layout = (ViewGroup) rootView.findViewById(R.id.layout_scores_personal);
			if (savedScores != null) {
				int length = savedScores.size(), lastDif = (int) savedScores.get(0);
				for (int i = 0; i < length; i += 3) {
					int dif, size, score;
					String t;
					dif = (int) savedScores.get(i);
					t = getString(R.string.text_dificultad) + " " + Dificultad.values()[dif].name();
					size = (int) savedScores.get(i + 1);
					t += "\n" + getString(R.string.text_tamanio) + " " + Tama�o.values()[size].name();
					score = (int) savedScores.get(i + 2);
					t += "\n" + getString(R.string.text_puntaje) + " " + score;
					TextView text = (TextView) inflater.inflate(R.layout.line_score_personal, null, false);
					text.setText(t);
					int id = getResources().getIdentifier("my_" + dif + "" + size, "drawable", getActivity().getPackageName());
					Drawable left = getResources().getDrawable(id);
					text.setCompoundDrawablesWithIntrinsicBounds(left, null, null, null);
					if (lastDif != dif)
						inflater.inflate(R.layout.divider, layout, true);
					layout.addView(text);
					inflater.inflate(R.layout.divider, layout, true);
					lastDif = dif;
				}
				layout.removeViewAt(layout.getChildCount() - 1);
			} else {
				View nothing = rootView.findViewById(R.id.text_nothing);
				nothing.setVisibility(View.VISIBLE);
			}
			return rootView;
		}
	}

	public static class WorldWideScore extends Fragment {
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.score_world_buscaminas, container, false);
			return rootView;
		}
	}

	public void clickSignIn(View v) {
		G_HELPER.beginUserInitiatedSignIn();
		v.setEnabled(false);
	}

	public void clickSignOut(View v) {
		G_HELPER.signOut();
		findViewById(R.id.layout_signedIn).setVisibility(View.GONE);
		findViewById(R.id.layout_notSignedIn).setVisibility(View.VISIBLE);
		setAutoSignIn(AUTO_SIGN_IN_OFF);
	}

	public void clickShowLeaderBoards(View v) {
		if (G_HELPER.isSignedIn())
			startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(G_HELPER.getApiClient()), 123);
	}

	public void clickShowAchievements(View v) {
		if (G_HELPER.isSignedIn())
			startActivityForResult(Games.Achievements.getAchievementsIntent(G_HELPER.getApiClient()), 1234);
	}

	public void clickEvaluate(View v) {
		String appPackageName = getPackageName();
		Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
		marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		startActivity(marketIntent);
	}

	public void onSignInFailed() {
		findViewById(R.id.button_signIn).setEnabled(true);
	}

	public void onSignInSucceeded() {
		findViewById(R.id.layout_notSignedIn).setVisibility(View.GONE);
		findViewById(R.id.layout_signedIn).setVisibility(View.VISIBLE);
		findViewById(R.id.button_signIn).setEnabled(true);
		setAutoSignIn(AUTO_SIGN_IN_ON);
	}

	private void setAutoSignIn(int autoSignIn) {
		SharedPreferences settings = getSharedPreferences(Configuracion.PREFS_CONFIG, 0);
		settings.edit().putInt(Configuracion.SIGN_IN, autoSignIn).commit();
	}

	public void onPageSelected(int position) {
		getSupportActionBar().setSelectedNavigationItem(position);
	}

	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		pager.setCurrentItem(arg0.getPosition());
	}

	public void onPageScrollStateChanged(int arg0) {
	}

	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
	}

	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
	}

}
