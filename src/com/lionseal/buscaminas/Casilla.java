package com.lionseal.buscaminas;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.lionseal.buscaminas.utiles.Cons;
import com.lionseal.buscaminas.utiles.Textures;

public class Casilla {
	public int x, y, valor;
	public boolean revealed = false, bandera = false;

	private AnimatedSprite casilla;
	private TableroBuscaminas tablero;

	public Casilla(TableroBuscaminas t, int cX, int cY) {
		tablero = t;
		x = cX;
		y = cY;
		float pX = x * Cons.CAS_WIDTH, pY = y * Cons.CAS_HEIGHT;
		valor = 0;

		VertexBufferObjectManager vbom = tablero.getVertexBufferObjectManager();
		casilla = new AnimatedSprite(pX, pY, Textures.get().tiledTexture(Cons.CASILLAS), vbom);
		casilla.setCurrentTileIndex(Cons.NORMAL.valor);
	}

	protected void selected() {
		if (!revealed && !bandera) {
			casilla.setCurrentTileIndex(Cons.INVERTIDA.valor);
		}
	}

	protected void unSelected() {
		if (!revealed && !bandera) {
			casilla.setCurrentTileIndex(Cons.NORMAL.valor);
		}
	}

	protected void doubleClicked() {
		if (!revealed && !bandera) {
			casilla.setCurrentTileIndex(valor);
			revealed = true;
		}
	}

	protected void setValor(int spriteValue, boolean r) {
		valor = spriteValue;
		revealed = r;
		if (revealed) {
			casilla.setCurrentTileIndex(valor);
		}
	}

	protected AnimatedSprite getSprite() {
		return casilla;
	}

	protected void setBandera() {
		casilla.setCurrentTileIndex(Cons.BANDERA.valor);
		bandera = true;
	}
}
