package com.lionseal.buscaminas;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.example.games.basegameutils.BaseGameActivity;
import com.lionseal.buscaminas.DBBuscaminas.Tableros;
import com.lionseal.buscaminas.utiles.GATracker;
import com.lionseal.buscaminas.utiles.GATracker.TrackerName;

public class MainBuscaminas extends BaseGameActivity {

	private String boom;
	private View iCara;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_buscaminas);
		boom = "OO";
		iCara = findViewById(R.id.image_carita);
		iCara.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(MainBuscaminas.this, "B" + boom + "M", Toast.LENGTH_SHORT).show();
				boom += "O";
			}
		});

		GATracker.getTracker(this, TrackerName.APP_TRACKER);
	}

	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	protected void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		if (response == Activity.RESULT_CANCELED) {
			SharedPreferences settings = getSharedPreferences(Configuracion.PREFS_CONFIG, 0);
			settings.edit().putInt(Configuracion.SIGN_IN, LeaderboardBuscaminas.AUTO_SIGN_IN_OFF).commit();
		}
	}

	protected void onResume() {
		super.onResume();
		SharedPreferences settings = getSharedPreferences(Configuracion.PREFS_CONFIG, 0);
		mHelper.setMaxAutoSignInAttempts(settings.getInt(Configuracion.SIGN_IN, LeaderboardBuscaminas.AUTO_SIGN_IN_ON));

		DBBuscaminas dbb = new DBBuscaminas(this);
		SQLiteDatabase db = dbb.getReadableDatabase();

		View vContinue = findViewById(R.id.button_continue);
		iCara = findViewById(R.id.image_carita);
		int dpValue = 4; // margin in dips
		float d = getResources().getDisplayMetrics().density;
		int margin = (int) (dpValue * d);
		LayoutParams params = new LayoutParams(0, LayoutParams.MATCH_PARENT);
		params.setMargins(0, 0, margin, 0);
		Cursor c = db.query(Tableros.TABLE_NAME, null, null, null, null, null, null);
		if (c.moveToFirst()) {
			vContinue.setVisibility(View.VISIBLE);
			params.weight = 1f;
			iCara.setLayoutParams(params);
		} else {
			vContinue.setVisibility(View.GONE);
			params.weight = 2f;
			iCara.setLayoutParams(params);
		}
		db.close();
	}

	public void clickStartGame(View v) {
		new Continuar(this, true);
	}

	public void clickSettings(View v) {
		Configuracion d = new Configuracion(this);
		d.show();
		Tracker myTracker = GATracker.getTracker(this, TrackerName.APP_TRACKER);
		myTracker.send(new HitBuilders.EventBuilder().setCategory("Main Screen").setAction("Button Settings").build());
	}

	public void clickScores(View v) {
		startActivity(new Intent(this, LeaderboardBuscaminas.class));
	}

	public void clickContinue(View v) {
		Continuar d = new Continuar(this, false);
		d.show();
		Tracker myTracker = GATracker.getTracker(this, TrackerName.APP_TRACKER);
		myTracker.send(new HitBuilders.EventBuilder().setCategory("Main Screen").setAction("Button Continue").build());
	}

	public void clickComoJugar(View v) {
		Intent i = new Intent(this, ComoJugar.class);
		startActivity(i);
		Tracker myTracker = GATracker.getTracker(this, TrackerName.APP_TRACKER);
		myTracker.send(new HitBuilders.EventBuilder().setCategory("Main Screen").setAction("Button How to play").build());
	}

	public void onSignInFailed() {
	}

	public void onSignInSucceeded() {
	}
}
