package com.lionseal.buscaminas;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.Stack;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.camera.ZoomCamera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.RepeatingSpriteBackground;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.batch.DynamicSpriteBatch;
import org.andengine.entity.sprite.batch.SpriteBatch;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.PinchZoomDetector;
import org.andengine.input.touch.detector.PinchZoomDetector.IPinchZoomDetectorListener;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;
import org.andengine.input.touch.detector.SurfaceScrollDetector;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.source.AssetBitmapTextureAtlasSource;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.lionseal.buscaminas.utiles.Cons;
import com.lionseal.buscaminas.utiles.Dificultad;
import com.lionseal.buscaminas.utiles.GATracker;
import com.lionseal.buscaminas.utiles.Tama�o;
import com.lionseal.buscaminas.utiles.Textures;
import com.lionseal.buscaminas.utiles.Utiles;
import com.lionseal.buscaminas.utiles.GATracker.TrackerName;

public class TableroBuscaminas extends SimpleBaseGameActivity implements IOnSceneTouchListener, IScrollDetectorListener, IPinchZoomDetectorListener {

	public static final String TT = "TAMA�O_TABLERO";
	public static final String SEED = "SEED";
	private float CAMERA_WIDTH, CAMERA_HEIGHT, TABLERO_WIDTH, TABLERO_HEIGHT, extraTop, minX, minY, maxX, maxY;
	private ZoomCamera camera;
	protected HUD hud;
	private RepeatingSpriteBackground background;
	private SurfaceScrollDetector mScrollDetector;
	private PinchZoomDetector mPinchZoomDetector;
	private float oldZoomFactor, minZoomFactor, maxZoomFactor;
	protected Scene scene;

	// google services
	private AdMob ads;
	private GameHelper G_HELPER;

	// ***********************//
	// ***VARIABLES TABLERO***//
	private Casilla[][] casillas;
	private int tWidth, tHeight, tMinas, difficulty, size;
	private ArrayList<Casilla> casillaSelected, minas;
	private boolean firstClick = true, gameOver = false, lose = false;
	private AreaReloj areaReloj;
	private AreaRestantes areaRestantes;
	private AnimatedSprite carita;
	private Explosiones explosiones;
	private long seed;
	private String previousSeed = null;

	// ***VARIABLES TABLERO***//
	// ***********************//

	protected void onCreate(Bundle s) {
		super.onCreate(s);
		ads = new AdMob(this);

		G_HELPER = new GameHelper(this, GameHelper.CLIENT_GAMES);
		G_HELPER.setup(new GameHelperListener() {
			public void onSignInSucceeded() {
			}

			public void onSignInFailed() {
			}
		});
		SharedPreferences settings = getSharedPreferences(Configuracion.PREFS_CONFIG, 0);
		G_HELPER.setMaxAutoSignInAttempts(settings.getInt(Configuracion.SIGN_IN, LeaderboardBuscaminas.AUTO_SIGN_IN_ON));

		GATracker.getTracker(this, TrackerName.APP_TRACKER);
	}

	protected void onStart() {
		super.onStart();
		ads.loadInterstitial();
		G_HELPER.onStart(this);
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	protected void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		G_HELPER.onActivityResult(request, response, data);
	}

	protected void onPause() {
		super.onPause();
		if (!gameOver && !firstClick) {
			DBBuscaminas dbb = new DBBuscaminas(this);
			dbb.saveScene(seed + "", difficulty, size, areaReloj.getTime(), minas.size(), casillas);
		}
	}

	protected void onStop() {
		super.onStop();
		G_HELPER.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	public EngineOptions onCreateEngineOptions() {
		Intent i = getIntent();
		previousSeed = i.getStringExtra(SEED);
		tWidth = i.getIntExtra(Configuracion.WIDTH, Tama�o.CHICO.width);
		tHeight = i.getIntExtra(Configuracion.HEIGHT, Tama�o.CHICO.height);
		tMinas = i.getIntExtra(Configuracion.MINAS, (int) (tWidth * tHeight / Dificultad.PRINCIPIANTE.proporcion));
		difficulty = i.getIntExtra(Configuracion.DIFFICULTY, Dificultad.PRINCIPIANTE.ordinal());
		size = i.getIntExtra(Configuracion.SIZE, Tama�o.CHICO.ordinal());

		DisplayMetrics d = getResources().getDisplayMetrics();

		TABLERO_WIDTH = tWidth * Cons.CAS_WIDTH;
		TABLERO_HEIGHT = tHeight * Cons.CAS_HEIGHT;
		CAMERA_WIDTH = d.widthPixels;
		CAMERA_HEIGHT = d.heightPixels;

		// aproximaci�n recursiva
		float minZoomY = CAMERA_HEIGHT / (TABLERO_HEIGHT + Cons.CAS_HEIGHT * 2);
		extraTop = (CAMERA_HEIGHT / 10f) / minZoomY;
		minZoomY = CAMERA_HEIGHT / (TABLERO_HEIGHT + Cons.CAS_HEIGHT * 2 + extraTop);
		extraTop = (CAMERA_HEIGHT / 10f) / minZoomY;
		minZoomY = CAMERA_HEIGHT / (TABLERO_HEIGHT + Cons.CAS_HEIGHT * 2 + extraTop);
		extraTop = (CAMERA_HEIGHT / 10f) / minZoomY;
		minZoomY = CAMERA_HEIGHT / (TABLERO_HEIGHT + Cons.CAS_HEIGHT * 2 + extraTop);
		extraTop = (CAMERA_HEIGHT / 10f) / minZoomY;
		minZoomY = CAMERA_HEIGHT / (TABLERO_HEIGHT + Cons.CAS_HEIGHT * 2 + extraTop);
		extraTop = (CAMERA_HEIGHT / 10f) / minZoomY;
		// fin aproximaci�n
		float minZoomX = CAMERA_WIDTH / (TABLERO_WIDTH + Cons.CAS_HEIGHT * 2);
		minZoomFactor = minZoomY < minZoomX ? minZoomY : minZoomX;
		maxZoomFactor = CAMERA_HEIGHT / (Cons.CAS_HEIGHT * 5.5f);

		minX = -Cons.CAS_WIDTH;
		minY = -Cons.CAS_HEIGHT - extraTop;
		maxX = TABLERO_WIDTH + Cons.CAS_WIDTH;
		maxY = TABLERO_HEIGHT + Cons.CAS_HEIGHT;
		camera = new SmoothCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT, TABLERO_WIDTH, TABLERO_HEIGHT, 10);
		camera.setBoundsEnabled(true);
		camera.setBounds(minX, minY, maxX, maxY);
		camera.setZoomFactor(minZoomFactor);
		camera.setCenter(TABLERO_WIDTH / 2, TABLERO_HEIGHT / 2 - extraTop);

		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), camera);
	}

	@Override
	protected void onCreateResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("images/");
		TextureManager tm = getTextureManager();
		TiledTextureRegion ttr;
		BitmapTextureAtlas texturas;
		int x = 0, y = 0;
		texturas = new BitmapTextureAtlas(tm, Cons.CAS_WIDTH * Cons.values().length, Cons.CAS_HEIGHT);
		ttr = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texturas, this, "casilla.png", x, y, 14, 1);
		Textures.get().putTiledTexture(Cons.CASILLAS, ttr);
		texturas.load();

		texturas = new BitmapTextureAtlas(tm, Cons.CARA_WIDTH * Cons.values().length, Cons.CARA_HEIGHT);
		ttr = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texturas, this, "caras.png", x, y, 4, 1);
		Textures.get().putTiledTexture(Cons.CARAS, ttr);
		texturas.load();

		texturas = new BitmapTextureAtlas(tm, Cons.REL_WIDTH * Cons.values().length, Cons.REL_HEIGHT);
		ttr = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texturas, this, "reloj.png", x, y, 12, 1);
		Textures.get().putTiledTexture(Cons.RELOJ, ttr);
		texturas.load();

		texturas = new BitmapTextureAtlas(tm, Cons.EXP_WIDTH * Cons.EXP_COL, Cons.EXP_HEIGHT * Cons.EXP_ROW);
		ttr = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texturas, this, "explosion_big.png", x, y, Cons.EXP_COL, 1);
		Textures.get().putTiledTexture(Cons.BIG.name(), ttr);
		ttr = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texturas, this, "explosion_small.png", x, y, Cons.EXP_COL, 1);
		Textures.get().putTiledTexture(Cons.SMALL.name(), ttr);
		texturas.load();

		background = new RepeatingSpriteBackground(CAMERA_WIDTH, CAMERA_HEIGHT, getTextureManager(), AssetBitmapTextureAtlasSource.create(getAssets(), "images/background.png"),
				getVertexBufferObjectManager());
	}

	protected Scene onCreateScene() {
		scene = new Scene();
		casillas = new Casilla[tWidth][tHeight];
		int tama�o = tWidth * tHeight;
		createHud(tama�o);
		minas = new ArrayList<>();
		casillaSelected = new ArrayList<>();
		if (previousSeed != null) {
			loadSavedScene();
		} else {
			loadNewScene();
		}
		final SpriteBatch sb = new DynamicSpriteBatch(0, 0, Textures.get().tiledTexture(Cons.CASILLAS).getTexture(), tama�o, getVertexBufferObjectManager()) {
			@Override
			public boolean onUpdateSpriteBatch() {
				for (Casilla[] cc : casillas) {
					for (Casilla c : cc) {
						draw(c.getSprite());
					}
				}
				return true;
			}
		};
		scene.attachChild(sb);
		scene.setBackground(background);
		scene.setOnSceneTouchListener(this);
		mScrollDetector = new SurfaceScrollDetector(this);
		mPinchZoomDetector = new PinchZoomDetector(this);
		return scene;
	}

	private void createHud(int tama�o) {
		hud = new HUD();

		float hudHeight = CAMERA_HEIGHT / 10f;
		float zoom = hudHeight / Cons.CAS_HEIGHT / 2f;
		Rectangle hudBG = new Rectangle(0, 0, CAMERA_WIDTH, hudHeight, getVertexBufferObjectManager());
		hudBG.setColor(0.84f, 0.84f, 0.84f);
		hud.attachChild(hudBG);
		Line divider = new Line(0, hudHeight + 1, CAMERA_WIDTH, hudHeight + 1, 4f, getVertexBufferObjectManager());
		divider.setColor(0.48f, 0.54f, 0.6f);
		hud.attachChild(divider);

		areaReloj = new AreaReloj(this, 0, 0, zoom);
		areaRestantes = new AreaRestantes(this, CAMERA_WIDTH - Cons.REL_WIDTH * zoom, Cons.REL_HEIGHT * zoom, zoom, tMinas, tama�o - tMinas);
		carita = new AnimatedSprite(0, 0, Textures.get().tiledTexture(Cons.CARAS), getVertexBufferObjectManager()) {
			public boolean onAreaTouched(final TouchEvent event, final float x, final float y) {
				restart();
				return true;
			}
		};
		carita.setScaleCenter(0, 0);
		carita.setScale(zoom);
		carita.setPosition(CAMERA_WIDTH / 2 - carita.getWidthScaled() / 2, 0);
		hud.attachChild(carita);

		camera.setHUD(hud);
	}

	private void loadSavedScene() {
		seed = Long.valueOf(previousSeed);
		DBBuscaminas dbb = new DBBuscaminas(this);
		ArrayList<Casilla> aCasillas = dbb.getSavedScene(previousSeed);
		for (Casilla c : aCasillas) {
			casillas[c.x][c.y] = c;
			if (c.valor == Cons.MINA.valor)
				minas.add(c);
			if (c.bandera)
				areaRestantes.descontarMinas();
			if (c.revealed)
				areaRestantes.descontarCasillas();
		}
		areaReloj.setTime(dbb.getSavedTime());
		Collections.shuffle(minas);
		firstClick = false;
		areaReloj.start();
	}

	private void loadNewScene() {
		seed = Calendar.getInstance().getTimeInMillis();
		for (int i = 0; i < tWidth; i++) {
			for (int j = 0; j < tHeight; j++) {
				casillas[i][j] = new Casilla(this, i, j);
			}
		}
	}

	private void firstClick(int clickX, int clickY) {
		// lugares disponibles para colocar minas
		int size = tWidth * tHeight;
		ArrayList<Integer> ranuras = new ArrayList<>();
		for (int r = 0; r < size; r++) {
			ranuras.add(r);
		}
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				Integer remover = clickX + i + ((clickY + j) * tWidth);
				try {
					ranuras.remove(remover);
				} catch (Exception e) {
				}
			}
		}

		// carga minas en forma aleatoria y los valores adyacentes
		Random aleatorio = new Random(seed);
		for (int i = 0; i < tMinas; i++) {
			int pos = aleatorio.nextInt(ranuras.size());
			int hayMina = ranuras.get(pos);
			ranuras.remove(pos);
			int cx = hayMina % tWidth, cy = hayMina / tWidth;
			casillas[cx][cy].setValor(Cons.MINA.valor, false);
			minas.add(casillas[cx][cy]);
			for (int w = cx - 1; w < cx + 2; w++) {
				for (int h = cy - 1; h < cy + 2; h++) {
					try {
						Casilla c = casillas[w][h];
						if (c.valor < 8) {
							c.setValor(c.valor + 1, false);
						}
					} catch (Exception e) {
					}
				}
			}
		}
		firstClick = false;
		areaReloj.start();
	}

	private void pressedCasilla(float x, float y) {
		if (x < 0 || y < 0 || gameOver)
			return;
		int cx = (int) x / Cons.CAS_WIDTH, cy = (int) y / Cons.CAS_HEIGHT;
		Casilla casillaNew;
		try {
			casillaNew = casillas[cx][cy];
		} catch (Exception e) {
			return;
		}
		if (!casillaSelected.contains(casillaNew)) {
			if (!casillaNew.bandera) {
				casillaSelected.add(casillaNew);
				if (casillaNew.revealed) {
					for (int i = cx - 1; i < cx + 2; i++) {
						for (int j = cy - 1; j < cy + 2; j++) {
							try {
								Casilla c = casillas[i][j];
								if (!c.revealed && !c.bandera && !c.equals(casillaNew)) {
									c.selected();
									casillaSelected.add(c);
								}
							} catch (Exception e) {
							}
						}
					}
				} else {
					casillaNew.selected();
				}
				setCarita(Cons.CARA_BOCA.valor);
			}
		}
	}

	private void releasedCasilla() {
		if (gameOver || casillaSelected.isEmpty())
			return;
		Casilla pressed = casillaSelected.get(0);
		if (pressed.revealed) {
			colocarBanderasMostrar(pressed);
		}
		setCarita(Cons.CARA_NORMAL.valor);
		clickedCasilla(pressed);
	}

	private void clickedCasilla(Casilla c) {
		if (gameOver)
			return;
		int x = c.x, y = c.y;
		if (firstClick)
			firstClick(x, y);
		if (!c.revealed) {
			c.doubleClicked();
			if (c.valor == Cons.MINA.valor) {
				c.setValor(Cons.ROJA.valor, true);
				explosiones = new Explosiones(this);
				explosiones.addExplosion(c, Cons.BIG.name());
				areaRestantes.descontarMinas();
				minas.remove(c);
				gameOver(false);
			} else {
				areaRestantes.descontarCasillas();
			}
			if (c.valor == Cons.NADA.valor) {
				mostrarVacios(c);
			}
		}
		if (areaRestantes.casillasRestantes == 0) {
			gameOver(true);
		}
		casillaSelected.clear();
	}

	private void mostrarVacios(Casilla casillaClicked) {
		HashMap<String, Casilla> mostrados = new HashMap<>();
		Stack<Casilla> sinMostrar = new Stack<>();
		sinMostrar.add(casillaClicked);
		while (!sinMostrar.isEmpty()) {
			Casilla casillaSinMostrar = sinMostrar.pop();
			if (!casillaSinMostrar.revealed) {
				casillaSinMostrar.doubleClicked();
				areaRestantes.descontarCasillas();
			}
			int x = casillaSinMostrar.x;
			int y = casillaSinMostrar.y;
			mostrados.put(x + "," + y, casillaSinMostrar);
			if (casillaSinMostrar.valor == Cons.NADA.valor) {
				for (int i = x - 1; i < x + 2; i++) {
					for (int j = y - 1; j < y + 2; j++) {
						try {
							Casilla c = casillas[i][j];
							if (mostrados.get(c.x + "," + c.y) == null) {
								sinMostrar.add(c);
							}
						} catch (Exception e) {
						}
					}
				}
			}
		}
	}

	private void colocarBanderasMostrar(Casilla casilla) {
		int x = casilla.x;
		int y = casilla.y;
		ArrayList<Casilla> sinBandera = new ArrayList<>();
		ArrayList<Casilla> conBandera = new ArrayList<>();
		for (int i = x - 1; i < x + 2; i++) {
			for (int j = y - 1; j < y + 2; j++) {
				try {
					Casilla c = casillas[i][j];
					if (!c.revealed) {
						if (c.bandera) {
							conBandera.add(c);
						} else {
							sinBandera.add(c);
						}
					}
				} catch (Exception e) {
				}
			}
		}
		if (sinBandera.size() == (casilla.valor - conBandera.size())) {
			for (Casilla c : sinBandera) {
				c.setBandera();
				areaRestantes.descontarMinas();
			}
		} else if (conBandera.size() == casilla.valor) {
			for (Casilla c : sinBandera) {
				clickedCasilla(c);
			}
		} else {
			for (Casilla c : sinBandera) {
				c.unSelected();
			}
		}
	}

	private void gameOver(boolean result) {
		if (gameOver)
			return;
		Log.d("Buscaminas", "gameOver");
		int index;
		String show;
		if (result) {
			index = Cons.CARA_GANO.valor;
			show = saveScore();
			(new Runnable() {
				public void run() {
					for (Casilla c : minas) {
						if (!c.bandera) {
							c.setBandera();
							areaRestantes.descontarMinas();
						}
					}
				}
			}).run();
		} else {
			lose = true;
			show = getResources().getString(R.string.show_lose);
			index = Cons.CARA_MUERTO.valor;
			(new Runnable() {
				public void run() {
					for (Casilla c : minas) {
						if (!c.bandera) {
							explosiones.addExplosion(c, Cons.SMALL.name());
						}
					}
				}
			}).run();
		}
		Tracker tracker = GATracker.getTracker(this, TrackerName.APP_TRACKER);
		tracker.send(new HitBuilders.EventBuilder().setCategory("Tablero").setAction("Game Over").setLabel(result ? "Win" : "Lose").build());
		setCarita(index);
		hud.registerTouchArea(carita);
		areaReloj.stop();
		gameOver = true;
		DBBuscaminas dbb = new DBBuscaminas(this);
		dbb.deleteScene(seed + "");
		double[] est = dbb.updateEstadisticas(difficulty, size, result);
		show += "\n" + getResources().getString(R.string.show_porcentaje) + " " + Math.round((est[0] / est[1]) * 100) + "%";
		Utiles.Toast(this, show);
		zoomChanged(minZoomFactor);
	}

	private void restart() {
		Log.d("Buscaminas", "restart");
		if (lose) {
			ads.showInterstitial();
		}
		hud.clearTouchAreas();
		scene.clearTouchAreas();
		scene.clearUpdateHandlers();
		scene.clearChildScene();
		mEngine.clearUpdateHandlers();
		firstClick = true;
		gameOver = false;
		lose = false;
		previousSeed = null;
		getEngine().setScene(onCreateScene());
	}

	private void setCarita(int index) {
		if (gameOver)
			return;
		carita.setCurrentTileIndex(index);
	}

	private String saveScore() {
		Log.d("Buscaminas", "saveScore");
		DBBuscaminas dbb = new DBBuscaminas(this);
		String show = getResources().getString(R.string.show_win);
		if (dbb.saveRecord(difficulty, size, areaReloj.getTime())) {
			show += "\n" + getResources().getString(R.string.show_better_score);
		} else {
			show += "\n" + getResources().getString(R.string.show_worse_score);
		}
		String name = "leaderboard_" + Dificultad.values()[difficulty].name().toLowerCase();
		int id = getResources().getIdentifier(name, "string", getPackageName());
		String leaderboardId = getResources().getString(id);
		Log.d("Submiting", name + ", " + leaderboardId);
		name = "achievement_" + Dificultad.values()[difficulty].name().toLowerCase();
		id = getResources().getIdentifier(name, "string", getPackageName());
		String achievementId = getResources().getString(id);
		if (G_HELPER.isSignedIn()) {
			Games.Leaderboards.submitScore(G_HELPER.getApiClient(), leaderboardId, (-10000 * (size - 5)) + areaReloj.getTime());
			Games.Achievements.unlock(G_HELPER.getApiClient(), achievementId);
			Log.d("Buscaminas", "Signed In");
		} else {
			Log.d("Buscaminas", "Signed Out");
		}

		return show;
	}

	@Override
	public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent event) {
		if (gameOver)
			return true;
		mPinchZoomDetector.onTouchEvent(event);
		if (mPinchZoomDetector.isZooming()) {
			mScrollDetector.setEnabled(false);
		} else {
			if (event.isActionDown()) {
				mScrollDetector.setEnabled(true);
				pressedCasilla(event.getX(), event.getY());
			}
			if (event.isActionUp()) {
				releasedCasilla();
			}
			mScrollDetector.onTouchEvent(event);
		}
		return true;
	}

	@Override
	public void onScrollStarted(final ScrollDetector pScollDetector, final int pPointerID, final float pDistanceX, final float pDistanceY) {
		final float zoomFactor = camera.getZoomFactor();
		camera.offsetCenter(-pDistanceX / zoomFactor, -pDistanceY / zoomFactor);
		if (!casillaSelected.isEmpty()) {
			for (Casilla c : casillaSelected) {
				if (!c.revealed)
					c.unSelected();
			}
			setCarita(Cons.CARA_NORMAL.valor);
			casillaSelected.clear();
		}
	}

	@Override
	public void onScroll(final ScrollDetector pScollDetector, final int pPointerID, final float pDistanceX, final float pDistanceY) {
		final float zoomFactor = camera.getZoomFactor();
		camera.offsetCenter(-pDistanceX / zoomFactor, -pDistanceY / zoomFactor);
	}

	@Override
	public void onScrollFinished(final ScrollDetector pScollDetector, final int pPointerID, final float pDistanceX, final float pDistanceY) {
		final float zoomFactor = camera.getZoomFactor();
		camera.offsetCenter(-pDistanceX / zoomFactor, -pDistanceY / zoomFactor);
	}

	@Override
	public void onPinchZoomStarted(final PinchZoomDetector pPinchZoomDetector, final TouchEvent pTouchEvent) {
		oldZoomFactor = camera.getZoomFactor();
		if (!casillaSelected.isEmpty()) {
			for (Casilla c : casillaSelected) {
				if (!c.revealed)
					c.unSelected();
			}
			setCarita(Cons.CARA_NORMAL.valor);
			casillaSelected.clear();
		}
	}

	public void onPinchZoom(final PinchZoomDetector pPinchZoomDetector, final TouchEvent pTouchEvent, final float pZoomFactor) {
		camera.setZoomFactor(oldZoomFactor * pZoomFactor);
	}

	public void onPinchZoomFinished(final PinchZoomDetector pPinchZoomDetector, final TouchEvent pTouchEvent, final float pZoomFactor) {
		zoomChanged(oldZoomFactor * pZoomFactor);
	}

	private void zoomChanged(float newZoomFactor) {
		if (newZoomFactor < minZoomFactor) {
			newZoomFactor = minZoomFactor;
			camera.setCenter(TABLERO_WIDTH / 2, TABLERO_HEIGHT / 2 - extraTop);
		} else if (newZoomFactor > maxZoomFactor) {
			newZoomFactor = maxZoomFactor;
		}
		camera.setZoomFactor(newZoomFactor);
		extraTop = (CAMERA_HEIGHT / 10f) / newZoomFactor;
		minY = -Cons.CAS_HEIGHT - extraTop;
		camera.setBounds(minX, minY, maxX, maxY);
	}
}
