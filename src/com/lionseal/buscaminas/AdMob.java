package com.lionseal.buscaminas;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class AdMob extends AdListener {
	private InterstitialAd mInterstitial;
	private Activity context;
	private int interval = 0;

	public AdMob(Activity c) {
		context = c;
		mInterstitial = new InterstitialAd(context);
		mInterstitial.setAdUnitId(context.getResources().getString(R.string.interstitial_id));
		mInterstitial.setAdListener(this);
	}

	public void loadInterstitial() {
		AdRequest adRequest;
		// adRequest = new
		// AdRequest.Builder().addTestDevice("45D6DA5747D7C84A143813D50F67910F").build();
		adRequest = new AdRequest.Builder().build();
		mInterstitial.loadAd(adRequest);
	}

	public void showInterstitial() {
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (interval < 1) {
					if (mInterstitial.isLoaded()) {
						mInterstitial.show();
						interval = 2;
					}
				} else {
					interval--;
				}
			}
		});
	}

	public void onAdLoaded() {
	}

	public void onAdFailedToLoad(int errorCode) {
		String errorReason = "No reason.";
		switch (errorCode) {
		case AdRequest.ERROR_CODE_INTERNAL_ERROR:
			errorReason = "Internal error";
			break;
		case AdRequest.ERROR_CODE_INVALID_REQUEST:
			errorReason = "Invalid request";
			break;
		case AdRequest.ERROR_CODE_NETWORK_ERROR:
			errorReason = "Network Error";
			break;
		case AdRequest.ERROR_CODE_NO_FILL:
			errorReason = "No fill";
			break;
		}
		Log.d("ADMOB", "Ad failed to load: " + errorReason);
	}

	public void onAdOpened() {
	}

	public void onAdClosed() {
	}

	public void onAdLeftApplication() {
	}
}
