package com.lionseal.buscaminas;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DBBuscaminas extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 14;
	public static final String DATABASE_NAME = "buscaminas";

	private Activity activity;
	private int time;

	public DBBuscaminas(Activity context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		activity = context;
	}

	protected ArrayList<Casilla> getSavedScene(String seed) {
		ArrayList<Casilla> casillas = new ArrayList<>();
		TableroBuscaminas tablero = (TableroBuscaminas) activity;
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(Tableros.TABLE_NAME, new String[] { Tableros.time }, Tableros.seed + "=?", new String[] { seed },
				null, null, null);
		c.moveToFirst();
		time = c.getInt(0);

		c = db.query(Casillas.TABLE_NAME, null, Casillas.seed_tablero + "=?", new String[] { seed }, null, null, null);
		while (c.moveToNext()) {
			int x = c.getInt(c.getColumnIndex(Casillas.x));
			int y = c.getInt(c.getColumnIndex(Casillas.y));
			int bottom = c.getInt(c.getColumnIndex(Casillas.valor));
			boolean revealed = c.getInt(c.getColumnIndex(Casillas.revealed)) > 0;
			boolean bandera = c.getInt(c.getColumnIndex(Casillas.bandera)) > 0;
			Casilla casilla = new Casilla(tablero, x, y);
			casilla.setValor(bottom, revealed);
			if (bandera)
				casilla.setBandera();
			casillas.add(casilla);
		}
		c.close();
		db.close();
		return casillas;
	}

	protected int getSavedTime() {
		return time;
	}

	protected void saveScene(String seed, int dif, int size, int time, int minas, Casilla[][] casillas) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Tableros.seed, seed);
		values.put(Tableros.dif, dif);
		values.put(Tableros.size, size);
		values.put(Tableros.time, time);
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss - dd/MM/yyyy");
		values.put(Tableros.date, df.format(Calendar.getInstance().getTime()));
		int sizeX = casillas.length;
		int sizeY = casillas[0].length;
		values.put(Tableros.width, sizeX);
		values.put(Tableros.height, sizeY);
		values.put(Tableros.minas, minas);
		db.beginTransaction();
		db.delete(Tableros.TABLE_NAME, Tableros.seed + "=?", new String[] { seed });
		db.delete(Casillas.TABLE_NAME, Casillas.seed_tablero + "=?", new String[] { seed });
		db.insert(Tableros.TABLE_NAME, null, values);
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				Casilla c = casillas[i][j];
				values = new ContentValues();
				values.put(Casillas.seed_tablero, seed);
				values.put(Casillas.x, c.x);
				values.put(Casillas.y, c.y);
				values.put(Casillas.revealed, c.revealed ? 1 : 0);
				values.put(Casillas.bandera, c.bandera ? 1 : 0);
				values.put(Casillas.valor, c.valor);
				db.insert(Casillas.TABLE_NAME, null, values);
			}
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
	}

	protected void deleteScene(String seed) {
		SQLiteDatabase db = getWritableDatabase();
		db.delete(Tableros.TABLE_NAME, Tableros.seed + "=?", new String[] { seed });
		db.delete(Casillas.TABLE_NAME, Casillas.seed_tablero + "=?", new String[] { seed });
		db.close();
	}

	protected boolean saveRecord(int difficulty, int size, int score) {
		SQLiteDatabase db = getWritableDatabase();
		Cursor c = db.query(Records.TABLE_NAME, new String[] { Records.id, Records.score }, Records.dif + "=? AND "
				+ Records.size + "=?", new String[] { difficulty + "", size + "" }, null, null, null);
		boolean betterScore = true;
		if (c.moveToFirst()) {
			int id = c.getInt(0);
			int savedScore = c.getInt(1);
			c.close();
			Log.d("CANT", "Records: " + savedScore + ", dif: " + difficulty + ", size: " + size);
			if (savedScore < score)
				betterScore = false;
			else
				db.delete(Records.TABLE_NAME, Records.id + "=?", new String[] { id + "" });
		}
		if (betterScore) {
			ContentValues values = new ContentValues();
			values.put(Records.dif, difficulty);
			values.put(Records.size, size);
			values.put(Records.score, score);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss - dd/MM/yyyy");
			values.put(Records.date, df.format(Calendar.getInstance().getTime()));
			db.beginTransaction();
			db.insert(Records.TABLE_NAME, null, values);
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		db.close();
		return betterScore;
	}

	protected double[] updateEstadisticas(int difficulty, int size, boolean result) {
		double[] estadistica = new double[2];
		SQLiteDatabase db = getWritableDatabase();
		Cursor c = db.query(Estadisticas.TABLE_NAME, new String[] { Estadisticas.win, Estadisticas.total, Estadisticas.id },
				Estadisticas.dif + "=? AND " + Estadisticas.size + "=?", new String[] { difficulty + "", size + "" }, null, null,
				null);
		int win = 0, total = 0;
		if (result)
			win++;
		total++;
		if (c.moveToFirst()) {
			win += c.getInt(0);
			total += c.getInt(1);
			int id = c.getInt(2);
			ContentValues values = new ContentValues();
			values.put(Estadisticas.win, win);
			values.put(Estadisticas.total, total);
			db.update(Estadisticas.TABLE_NAME, values, Estadisticas.id + "=?", new String[] { id + "" });
		} else {
			ContentValues values = new ContentValues();
			values.put(Estadisticas.dif, difficulty);
			values.put(Estadisticas.size, size);
			values.put(Estadisticas.win, win);
			values.put(Estadisticas.total, total);
			db.beginTransaction();
			db.insert(Estadisticas.TABLE_NAME, null, values);
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		db.close();
		estadistica[0] = win;
		estadistica[1] = total;
		Log.d("Buscaminas", "Estadistica:" + win + ", " + total);
		return estadistica;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_TABLERO);
		db.execSQL(SQL_CREATE_CASILLA);
		db.execSQL(SQL_CREATE_RECORDS);
		db.execSQL(SQL_CREATE_ESTADISTICAS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DROP_TABLERO);
		db.execSQL(SQL_DROP_CASILLA);
		db.execSQL(SQL_DROP_RECORDS);
		db.execSQL(SQL_DROP_ESTADISTICAS);
		onCreate(db);
	}

	public static final String SQL_CREATE_TABLERO = "CREATE TABLE " + Tableros.TABLE_NAME + " (" + Tableros.seed
			+ " LONG PRIMARY KEY," + Tableros.dif + " INTEGER NOT NULL," + Tableros.size + " INTEGER NOT NULL," + Tableros.time
			+ " INTEGER NOT NULL," + Tableros.date + " VARCHAR(20) NOT NULL," + Tableros.width + " INTEGER NOT NULL,"
			+ Tableros.height + " INTEGER NOT NULL," + Tableros.minas + " INTEGER NOT NULL)";

	public static final String SQL_DROP_TABLERO = "DROP TABLE IF EXISTS " + Tableros.TABLE_NAME;

	public static abstract class Tableros implements BaseColumns {
		public static final String TABLE_NAME = "tablero";
		public static final String seed = "seed";
		public static final String dif = "dif";
		public static final String size = "size";
		public static final String time = "time";
		public static final String date = "date";
		public static final String width = "width";
		public static final String height = "height";
		public static final String minas = "minas";
	}

	public static final String SQL_CREATE_CASILLA = "CREATE TABLE " + Casillas.TABLE_NAME + " (" + Casillas.id
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + Casillas.seed_tablero + " LONG REFERENCES " + Tableros.TABLE_NAME + "("
			+ Tableros.seed + ") ON DELETE CASCADE," + Casillas.x + " INTEGER NOT NULL," + Casillas.y + " INTEGER NOT NULL,"
			+ Casillas.valor + " INTEGER NOT NULL," + Casillas.revealed + " BOOLEAN NOT NULL," + Casillas.bandera
			+ " BOOLEAN NOT NULL)";

	public static final String SQL_DROP_CASILLA = "DROP TABLE IF EXISTS " + Casillas.TABLE_NAME;

	public static abstract class Casillas implements BaseColumns {
		public static final String TABLE_NAME = "casilla";
		public static final String id = "id";
		public static final String seed_tablero = "seed_tablero";
		public static final String x = "x";
		public static final String y = "y";
		public static final String valor = "valor";
		public static final String revealed = "revealed";
		public static final String bandera = "bandera";
	}

	public static final String SQL_CREATE_RECORDS = "CREATE TABLE " + Records.TABLE_NAME + " (" + Records.id
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + Records.dif + " INTEGER NOT NULL," + Records.size + " INTEGER NOT NULL,"
			+ Records.score + " INTEGER NOT NULL," + Records.date + " VARCHAR(20) NOT NULL)";

	public static final String SQL_DROP_RECORDS = "DROP TABLE IF EXISTS " + Records.TABLE_NAME;

	public static abstract class Records implements BaseColumns {
		public static final String TABLE_NAME = "records";
		public static final String id = "id";
		public static final String dif = "dif";
		public static final String size = "size";
		public static final String score = "score";
		public static final String date = "date";
	}

	public static final String SQL_CREATE_ESTADISTICAS = "CREATE TABLE " + Estadisticas.TABLE_NAME + " (" + Estadisticas.id
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + Estadisticas.dif + " INTEGER NOT NULL," + Estadisticas.size
			+ " INTEGER NOT NULL," + Estadisticas.win + " INTEGER NOT NULL," + Estadisticas.total + " INTEGER NOT NULL)";

	public static final String SQL_DROP_ESTADISTICAS = "DROP TABLE IF EXISTS " + Estadisticas.TABLE_NAME;

	public static abstract class Estadisticas implements BaseColumns {
		public static final String TABLE_NAME = "estadisticas";
		public static final String id = "id";
		public static final String dif = "dif";
		public static final String size = "size";
		public static final String win = "win";
		public static final String total = "total";
	}
}
